<!--

Title should be: Experience Recommendations - {{Stage Group}} FY{{YY}}-Q{{quarter number}} - {{Title or Description of the Evaluated Workflow / JTBD}}
(e.g. “Experience Recommendations - Create:Source Code FY21-Q1 - Obtaining screenshots from testing artifacts”)

-->

- **UX Scorecard Part 1**: {{add link to UX scorecard issue}}
- **Resulting Recommendations**: {{when ready, add a link to your recommendations epic}}

## Experience Recommendations Checklist

[Learn more about UX Scorecards](https://about.gitlab.com/handbook/engineering/ux/ux-scorecards/)

1. [ ] Add this issue to the stage group epic for the corresponding quarter's UX scorecards.
1. [ ] Brainstorm opportunities to fix or improve areas of the experience.
   - Use the findings from the Emotional Grading scale to determine areas of immediate focus. For example, if parts of the experience received a “Negative” Emotional Grade, consider addressing those first. 
1. [ ] Create an issue for each recommendation using one of the [Actionable Insight templates](/handbook/engineering/ux/ux-research-training/research-insights/#how-to-document-actionable-insights) in the GitLab project, depending on if it relates to a [product change](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Actionable%20Insight%20-%20Product%20change.md) or [needs more exploration](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Actionable%20Insight%20-%20Exploration%20needed.md). Alternatively, you can create a separate epic to hold all your recommendations. Add a `UX scorecard-rec` label to every issue or epic for traceability. To help with prioritization, add a [severity label](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity) to communicate appropriate urgency and impact to the experience. Link to the epic or issues here.
   - Recommendations do not need to be documented in your Dovetail project.
1. [ ] Think iteratively, and create dependencies where appropriate, remembering that sometimes the order of what we release is just as important as what we release.
   - If you need to break recommendations into phases or over multiple milestones, create multiple epics and use the [Category Maturity Definitions](https://about.gitlab.com/direction/maturity/) in the title of each epic: **Minimal, Viable, Complete, or Lovable**.
